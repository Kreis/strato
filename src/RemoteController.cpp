#include "RemoteController.h"

RemoteController::RemoteController() {
  lastSID = GlobalConfig::get()->gstate.lastSerializeId - 1;
}
RemoteController::~RemoteController() {}

void RemoteController::update(float deltatime,
  std::vector<std::shared_ptr<Aplayer> >& aplayers) {

  uint32_t newSID = GlobalConfig::get()->gstate.lastSerializeId;
  if (newSID == lastSID) {
    return;
  }
  lastSID = newSID;

  GenericState& gstate = GlobalConfig::get()->gstate;

  for (int32_t i = 0; i < 4; i++) {
    NetUser& user = gstate.users[ i ];

    if ( ! user.assigned) {
      aplayers[ i ]->assigned = false;
      continue;
    }

    if (user.username == GlobalConfig::get()->myusername) {
      GlobalConfig::get()->assignId = i + 1;
      continue;
    }

    aplayers[ i ]->assigned = true;
    if (user.moved) {
      aplayers[ i ]->moveTo({ user.lobbyX * 32, user.lobbyY * 32 });
    }
  }
}




