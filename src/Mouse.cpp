#include "Mouse.h"

std::unique_ptr<Mouse> Mouse::instancePtr = nullptr;

Mouse::Mouse() {}
Mouse::~Mouse() {}

Mouse* Mouse::get() {
  if (instancePtr == nullptr) {
    instancePtr = std::unique_ptr<Mouse>(new Mouse());
  }

  return instancePtr.get();
}

void Mouse::addObserver(std::weak_ptr<MouseObserver>& mo) {
  observers.push_back(mo);
}

void Mouse::notifyObservers() {
  
  Vec<int32_t> pos = InputState::get()->getMousePosition();
  bool leftC = InputState::get()->isTyped(MouseButton::Left);
  bool rightC = InputState::get()->isTyped(MouseButton::Right);
  
  
  for (auto it = observers.begin(); it != observers.end(); /*no increment*/) {
    if ((*it).expired()) {
      it = observers.erase(it);
      continue;
    }

    std::shared_ptr<MouseObserver> moLock = (*it).lock();
    moLock->movingOver(pos);
    if (leftC) {
      moLock->leftClick(pos);
    }
    if (rightC) {
      moLock->rightClick(pos);
    }

    it++;
  }
}



