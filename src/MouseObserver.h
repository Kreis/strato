#ifndef _MOUSE_OBSERVER_H_
#define _MOUSE_OBSERVER_H_

#include <memory>

#include "hz/Hz.h"

// Important!
// When initialize the sub class of this one,
// it has to be a shared_ptr in order to let it use, weak_from_this
class MouseObserver : public std::enable_shared_from_this<MouseObserver> {
public:
  virtual void leftClick(const Vec<int32_t>& p) = 0;
  virtual void rightClick(const Vec<int32_t>& p) = 0;
  virtual void movingOver(const Vec<int32_t>& p) = 0;
};

#endif
