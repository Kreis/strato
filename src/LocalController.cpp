#include "LocalController.h"

LocalController::LocalController() {
  initialized = false;
}
LocalController::~LocalController() {}

void LocalController::init(std::shared_ptr<Aplayer> aplayer) {
  std::cout << "init local controller" << std::endl;

  this->aplayer = aplayer;
  this->aplayer->assigned = true;

  Mouse::get()->addObserver(weak_from_this());

  Rect<int32_t> color = { 170, 170, 170, 180 };
  Rect<int32_t> bounds = { 0, 0, 32, 32 };
  lightTile.init("lightTile", color, bounds);

  initialized = true;
}

void LocalController::leftClick(const Vec<int32_t>& p) {
  if ( ! initialized) {
    return;
  }
  std::cout << "left click in local controller" << std::endl;


  Vec<int32_t> coord = getMapTile(p);

  std::vector<uint8_t> data;
  data.push_back(M_MOVE_TO);
  data.push_back((uint8_t)coord.x);
  data.push_back((uint8_t)coord.y);
  FactoryNet::get()->net()->sendMessage(data);

  coord.x *= 32;
  coord.y *= 32;
  aplayer->moveTo(coord);
}

void LocalController::rightClick(const Vec<int32_t>& p) {
  if ( ! initialized) {
    return;
  }

}

void LocalController::movingOver(const Vec<int32_t>& p) {
  if ( ! initialized) {
    return;
  }

  Vec<int32_t> coord = getMapTile(p);
  lightTile.getSprite()->bounds.x = 32 * coord.x;
  lightTile.getSprite()->bounds.y = 32 * coord.y;
}

void LocalController::update(float deltatime) {

  if ( ! initialized) {
    return;
  }

  SpriteBatch::get()->draw(lightTile.getSprite());
}

Vec<int32_t> LocalController::getMapTile(const Vec<int32_t>& p) {
  int32_t x = p.x / 32;
  int32_t y = p.y / 32;
  return {x, y};
}

bool LocalController::isInitialized() {
  return initialized;
}



