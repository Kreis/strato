#include "Lobby.h"

Lobby::Lobby() {}
Lobby::~Lobby() {}

void Lobby::init() {
  std::cout << "Lobby init" << std::endl;

  Resources::get()->loadMap("data/maps/lobby.tmj");
  Map* lobbyMap = Resources::get()->getMap("data/maps/lobby.tmj");

  SpriteBatch::get()->fixVecVertex("data/textures/tiles.png",
    lobbyMap->getVecVertex());

  for (int32_t i = 0; i < 4; i++) {
    aplayers.push_back(std::make_shared<Aplayer>());
  }
  for (int32_t i = 0; i < lobbyMap->objectsName.size(); i++) {
    std::string oname = lobbyMap->objectsName[ i ];
    Rect<int32_t> orect = lobbyMap->objectsRect[ i ];
    if (oname == "body1") {
      aplayers[ 0 ]->init({ 30, 210, 30, 255 }, orect); // green
    }
    if (oname == "body2") {
      aplayers[ 1 ]->init({ 210, 30, 30, 255 }, orect); // red
    }
    if (oname == "body3") {
      aplayers[ 2 ]->init({ 30, 30, 210, 255 }, orect); // blue
    }
    if (oname == "body4") {
      aplayers[ 3 ]->init({ 255, 174, 201, 255 }, orect); // pink
    }
    if (oname == "hole5") {
      confirmRect = orect; // confirmation hole
    }
  }

  localController = std::make_shared<LocalController>();

  // net connection
  FactoryNet::get()->net()->initConnect("netinfo.txt");

  ready = false;
}

void Lobby::update(float deltatime) {

  if ( ! localController->isInitialized()) {
    for (int32_t i = 0; i < 4; i++) {
      if (aplayers[ i ]->clicked) {
        aplayers[ i ]->clicked = false;
        std::vector<uint8_t> data;
        data.push_back(M_ASSIGN);
        data.push_back((uint8_t)(i + 1));
        for (char c : GlobalConfig::get()->myusername) {
          data.push_back(c);
        }
        FactoryNet::get()->net()->sendMessage(data);
        break;
      }
    }
    uint32_t assignId = GlobalConfig::get()->assignId;
    if (assignId > 0) {
      localController->init(aplayers[ assignId - 1 ]);
    }
  }

  remoteController.update(deltatime, aplayers);

  localController->update(deltatime);
  for (int32_t i = 0; i < 4; i++) {
    aplayers[ i ]->update(deltatime);
  }

  int32_t assignedNum = 0;
  int32_t readyNum = 0;
  for (const std::shared_ptr<Aplayer>& p : aplayers) {
    assignedNum += p->assigned ? 1 : 0;
    Rect<int32_t> x = p->bounds;
    readyNum += confirmRect.contains(x) ? 1 : 0;
  }
  if (assignedNum >= 2 && assignedNum == readyNum) {
    ready = true;
    std::vector<uint8_t> m;
    m.push_back(M_READY);
    FactoryNet::get()->net()->sendMessage(m);
  }
}

void Lobby::draw() {
  for (int32_t i = 0; i < 4; i++) {
    aplayers[ i ]->draw();
  }
}

