#ifndef _LOBBY_H_
#define _LOBBY_H_

#include <vector>

#include "hz/Hz.h"

#include "LocalController.h"
#include "RemoteController.h"
#include "Aplayer.h"
#include "GenericState.h"
#include "GlobalConfig.h"
#include "FactoryNet.h"

class Lobby {
public:
  Lobby();
  ~Lobby();

  void init();
  void update(float deltatime);
  void draw();

  std::shared_ptr<LocalController> localController;
  RemoteController remoteController;
  std::vector<std::shared_ptr<Aplayer> > aplayers;

  Rect<int32_t> confirmRect;
  bool ready;
};


#endif
