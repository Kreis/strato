#ifndef _UNIT_H_
#define _UNIT_H_

#include "hz/Hz.h"

#include "GlobalConfig.h"

class Unit {
public:
  Unit();
  ~Unit();

  void init(uint32_t id, Vec<int32_t> pos, int32_t unitType);
  void update(float deltatime);
  void draw();

  void moveTo(Vec<int32_t> p);
  
  int32_t unitType;
  int32_t animatorSession;

  Rect<int32_t> color;
  float speed;
  bool selected;
  bool active;
  Vec<float> movingTo;
  Rect<float> bounds;
  uint32_t id;
};


#endif
