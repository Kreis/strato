#ifndef _GLOBAL_CONFIG_H_
#define _GLOBAL_CONFIG_H_

#include <memory>
#include <iostream>

#include "hz/Hz.h"

#include "GenericState.h"

class GlobalConfig {
public:
  ~GlobalConfig();

  static GlobalConfig* get();

  std::string myusername = "unknown";
  uint32_t assignId = 0;

  Rect<int32_t> colorGreen = { 30, 210, 30, 255 };
  Rect<int32_t> colorRed = { 210, 30, 30, 255 }; 
  Rect<int32_t> colorBlue = { 30, 30, 210, 255 }; 
  Rect<int32_t> colorPink = { 255, 174, 201, 255 }; 

  GenericState gstate;

private:
  GlobalConfig();
  static std::unique_ptr<GlobalConfig> instancePtr;
};

#endif
