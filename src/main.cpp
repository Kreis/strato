#include <iostream>

#include "hz/Hz.h"

#include "Game.h"
#include "GlobalConfig.h"

int main(int argc, char** argv) {

  std::cout << "start strato" << std::endl;

  std::cout << "Ingrese su nombre de usuario" << std::endl;
  std::cin >> GlobalConfig::get()->myusername;

  Window window;
  window.start("Strato", 800, 544);

  std::vector<std::string> texturePriority;
  texturePriority.push_back("data/textures/tiles.png");
  texturePriority.push_back("lightTile");
  texturePriority.push_back("data/textures/GlobalTexture.png");
  texturePriority.push_back("data/textures/GlobalMenu.png");
  SpriteBatch::get()->init(texturePriority);

  SpriteBatch::get()->setNullTextureVecVertex("lightTile");
  SpriteBatch::get()->
    setCameraFreeVecVertex("data/textures/GlobalMenu.png", true);

  Resources::get()->loadTextureAtlas("data/textures/GlobalTexture.json");
  Resources::get()->loadTextureAtlas("data/textures/GlobalMenu.json");

  Game game;
  game.init();

  while (true) {

    window.update();
    if ( ! window.isOpen()) {
      break;
    }

    game.update(window.getElapsedTime());
    game.draw();

    SpriteBatch::get()->flush(&window);
  }


  return 0;
}



