#ifndef _MOUSE_H_
#define _MOUSE_H_

#include <memory>
#include <list>

#include "MouseObserver.h"

class Mouse {
public:
  ~Mouse();
  static Mouse* get();

  void addObserver(std::weak_ptr<MouseObserver>& mo);
  
  void notifyObservers();

private:
  Mouse();
  static std::unique_ptr<Mouse> instancePtr;
  std::list<std::weak_ptr<MouseObserver> > observers;
};


#endif
