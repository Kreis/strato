#ifndef _APLAYER_H_
#define _APLAYER_H_

#include "hz/Hz.h"

#include "MouseObserver.h"
#include "Mouse.h"

class Aplayer : public MouseObserver {
public:
  Aplayer();
  ~Aplayer();

  void leftClick(const Vec<int32_t>& p) override;
  void rightClick(const Vec<int32_t>& p) override;
  void movingOver(const Vec<int32_t>& p) override;

  // Controlls
  void init(Rect<int32_t> color, Rect<int32_t> rect);
  void moveTo(Vec<int32_t> p);
  void setPosition(Vec<int32_t> p);

  void update(float deltatime);
  void draw();
  
  float speed;

  Rect<float> bounds;
  Vec<int32_t> movingTo;

  bool clicked;
  bool assigned;

private:
  Rect<int32_t> color;
  int32_t animatorSession;
  Vec<int32_t> defaultPosition;
};


#endif
