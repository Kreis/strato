#include "Field.h"

Field::Field() {}
Field::~Field() {}

void Field::init() {
  std::cout << "Field init" << std::endl;
  Resources::get()->loadMap("data/maps/field01.tmj");

  Map* fieldMap = Resources::get()->getMap("data/maps/field01.tmj");
  SpriteBatch::get()->fixVecVertex("data/textures/tiles.png",
    fieldMap->getVecVertex());

  Rect<int32_t> firstUnitRect;
  uint32_t assignId = GlobalConfig::get()->assignId;
  std::string assignString = "body" + std::to_string(assignId);
  for (int32_t i = 0; i < fieldMap->objectsName.size(); i++) {
    std::string oname = fieldMap->objectsName[ i ];
    Rect<int32_t> orect = fieldMap->objectsRect[ i ];

    ResType type = ResType::NOT_RES;
    if (oname == "rgreen") {
      type = ResType::GREEN;
    }
    if (oname == "rred") {
      type = ResType::RED;
    }

    if (type != ResType::NOT_RES) {
      Res r;
      r.position = orect.xy / 32;
      r.type = type;
      r.amount = 20;
      res.push_back(r);
    }

    if (oname == assignString) {
      firstUnitRect = orect;
    }
  }

  imageRes.init(
    "data/textures/GlobalTexture.json",
    "Resource_t.png",
    { 0, 0, 32, 32 });

  updateUnits();

  int32_t initialUnit = getNewUnit();
  Vec<uint8_t> firstUnitCoord = firstUnitRect.xy / 32;
  activeUnit(units[ initialUnit ].id, firstUnitCoord.x, firstUnitCoord.y);

  Mouse::get()->addObserver(weak_from_this());
  selectedMenu = false;
  selectedUnit = false;
  cameraSpeed = 600;

  Rect<int32_t> color = { 170, 170, 170, 180 };
  Rect<int32_t> bounds = { 0, 0, 32, 32 };
  lightTile.init("lightTile", color, bounds);

  Vec<int32_t> screenSize = InputState::get()->getCurrentScreenSize();
  Camera::get()->bounds.xy = firstUnitRect.xy + 16 - screenSize / 2;

  imageDuplicate.init("data/textures/GlobalMenu.json",
    "duplicate_t.png", { 700, 0, 64, 64 });

}

void Field::leftClick(const Vec<int32_t>& p) {
  selectedMenu = false;
  Vec<int32_t> pScreen = p + Camera::get()->bounds.xy;
  for (Unit& unit : units) {
    if (unit.bounds.contains(pScreen)) {
      unit.selected = true;
      selectedMenu = true;
    }
  }
}

void Field::rightClick(const Vec<int32_t>& p) {

}

void Field::movingOver(const Vec<int32_t>& p) {

  movingHor = 0;
  if (p.x < 0) {
    movingHor = -1;
  } else if (p.x > InputState::get()->getCurrentScreenSize().x) {
    // TODO test this in resolution changed
    movingHor = 1;
  }
  movingVer = 0;
  if (p.y < 0) {
    movingVer = -1;
  } else if (p.y > InputState::get()->getCurrentScreenSize().y) {
    movingVer = 1;
  }

  // check menu first
  if (imageDuplicate.getSprite()->bounds.contains(p)) {
    return;
  }


  // look coordinate for light tile
  Vec<int32_t> pScreen = p + Camera::get()->bounds.xy;
  Vec<int32_t> coord = pScreen / 32;
  if (coord.x < 0 || coord.y < 0 || coord.x >= 50 || coord.y >= 38) {
    return;
  }
  lightTile.getSprite()->bounds.x = 32 * coord.x;
  lightTile.getSprite()->bounds.y = 32 * coord.y;

}

void Field::update(float deltatime) {

  updateUnits();

  for (Unit& unit : units) {
    if ( ! unit.active) {
      continue;
    }
    unit.update(deltatime);
  }

  if (selectedMenu) {
    movingHor = 0;
    movingVer = 0;
  } else {
    SpriteBatch::get()->draw(lightTile.getSprite());
  }

  Camera::get()->bounds.x += (float)movingHor * cameraSpeed * deltatime;
  Camera::get()->bounds.y += (float)movingVer * cameraSpeed * deltatime;

}

void Field::draw() {
  for (Res& r : res) {
    imageRes.getSprite()->bounds.xy = r.position * 32;
    if (r.type == ResType::GREEN) {
      imageRes.getSprite()->color = { 60, 210, 60, 255 };
    }
    if (r.type == ResType::RED) {
      imageRes.getSprite()->color = { 210, 60, 60, 255 };
    }

    SpriteBatch::get()->draw(imageRes.getSprite());
  }

  for (Unit& unit : units) {
    if ( ! unit.active) {
      continue;
    }

    unit.draw();
  }

  if (selectedMenu) {
    SpriteBatch::get()->draw(imageDuplicate.getSprite());
  }
}

void Field::updateUnits() {

  for (int32_t i = 0; i < 4; i++) {
    NetUser& netUser = GlobalConfig::get()->gstate.users[ i ];
    if ( ! netUser.assigned) {
      continue;
    }

    for (NetUnit& nu : netUser.units) {
      if (unitsMap[ nu.id ] == 0) {
        this->units.push_back(Unit());
        unitsMap[ nu.id ] = units.size();
        units[ unitsMap[ nu.id ] - 1 ].init(nu.id, { 0, 0 }, i + 1);
      }
      if (nu.active) {
        Unit& unit = units[ unitsMap[ nu.id ] - 1 ];
        unit.active = true;
        Vec<int32_t> mt = { nu.x, nu.y };
        unit.bounds.xy = mt * 32;
        unit.moveTo(mt * 32);
      }
    }
  }
}

void Field::activeUnit(int32_t i, uint8_t x, uint8_t y) {
  std::vector<uint8_t> m;
  m.push_back(M_ACTIVE_UNIT);
  // identifier
  m.push_back(FactoryNet::get()->getB01(i));
  m.push_back(FactoryNet::get()->getB02(i));
  m.push_back(FactoryNet::get()->getB03(i));
  m.push_back(FactoryNet::get()->getB04(i));

  m.push_back(x);
  m.push_back(y);

  FactoryNet::get()->net()->sendMessage(m);
}

int32_t Field::getNewUnit() {
  for (int32_t i = 0; i < units.size(); i++) {
    Unit& unit = units[ i ];
    if (unit.unitType != GlobalConfig::get()->assignId) {
      continue;
    }

    if ( ! unit.active) {
      return i;
    }
  }

  return -1;
}





