#include "MockNetClient.h"


MockNetClient::MockNetClient() {
  connected = false;
}
MockNetClient::~MockNetClient() {}

std::unique_ptr<MockNetClient> MockNetClient::instancePtr = nullptr;

MockNetClient* MockNetClient::get() {
  if (instancePtr == nullptr) {
    instancePtr = std::unique_ptr<MockNetClient>(new MockNetClient());
  }

  return instancePtr.get();
}


std::vector<uint8_t> MockNetClient::getReceived() {
  std::vector<uint8_t> data;
  gstate.serialize(data);

  return data;
}

bool MockNetClient::initConnect(std::string netinfo_path) {
  connected = true;
  gstate.users[ 3 ].username = "mockedUser";
  gstate.users[ 3 ].assigned = true;
  gstate.users[ 3 ].lobbyX = 13;
  gstate.users[ 3 ].lobbyY = 14;
  gstate.users[ 3 ].moved = true;
  gstate.users[ 3 ].confirmed = true;
  return true;
}


bool MockNetClient::sendMessage(std::vector<uint8_t> data) {
  
  if (data[ 0 ] == M_ASSIGN) {
    localId = data[ 1 ] - 1;
    gstate.users[ localId ].username = GlobalConfig::get()->myusername;
    gstate.users[ localId ].assigned = true;
  }
  if (data[ 0 ] == M_MOVE_TO) {

    if (localId == -1) {
      return true;
    }

    gstate.users[ localId ].lobbyX = data[ 1 ];
    gstate.users[ localId ].lobbyY = data[ 2 ];
    gstate.users[ localId ].moved = true;
  }
  if (data[ 0 ] == M_READY) {
    gstate.state = 2;
    for (int32_t i = 0; i < 4; i++) {
      NetUser& netUser = gstate.users[ i ];
      if ( ! netUser.assigned) {
        continue;
      }
      NetUnit netUnit;
      netUnit.id = netUnitGenId++;
      netUnit.active = false;
      netUser.units.push_back(netUnit);
    }

    gstate.users[ 3 ].units[ 0 ].id = netUnitGenId++;
    gstate.users[ 3 ].units[ 0 ].active = true;
    gstate.users[ 3 ].units[ 0 ].x = 44;
    gstate.users[ 3 ].units[ 0 ].y = 32;
  }
  if (data[ 0 ] == M_ACTIVE_UNIT) {
    int32_t id = 0;
    id += (data[ 1 ] << 24);
    id += (data[ 2 ] << 16);
    id += (data[ 3 ] << 8);
    id += (data[ 4 ] << 0);

    uint8_t x = data[ 5 ];
    uint8_t y = data[ 6 ];
    
    for (NetUser& netUser : gstate.users) {
      for (NetUnit& netUnit : netUser.units) {
        if (netUnit.id == id) {
          netUnit.active = true;
          netUnit.x = x;
          netUnit.y = y;
        }
      }
    }
  }

  return true;
}

void MockNetClient::closeConnection() {
}
