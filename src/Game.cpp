#include "Game.h"

Game::Game() {}
Game::~Game() {}

void Game::init() {
  std::cout << "Game init" << std::endl;

  lobby.init();

  stage = 0;

  field = std::make_shared<Field>();
}

void Game::update(float deltatime) {

  std::vector<uint8_t> data = FactoryNet::get()->net()->getReceived();
  if (data.size() > 0) {
    GlobalConfig::get()->gstate.deserialize(data);
  }
  Mouse::get()->notifyObservers();

  if (GlobalConfig::get()->gstate.state == 2 && stage == 0) {
    std::cout << "READY!!" << std::endl;
    stage = 1;
    //lobby.localController = nullptr;
    field->init();
  }

  if (stage == 0) {
    lobby.update(deltatime);
  } else if (stage == 1) {
    field->update(deltatime);
  }
}

void Game::draw() {
  if (stage == 0) {
    lobby.draw();
  } else if (stage == 1) {
    field->draw();
  }
}



