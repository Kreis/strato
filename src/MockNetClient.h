#ifndef _MOCK_NETCLIENT_H_
#define _MOCK_NETCLIENT_H_

#include <memory>
#include <queue>

#include "hz/Hz.h"
#include "GenericState.h"
#include "GlobalConfig.h"

class MockNetClient {
public:
  ~MockNetClient();

  static MockNetClient* get();

  bool initConnect(std::string netinfo_path);
  bool sendMessage(std::vector<uint8_t> data);
  void closeConnection();
  std::vector<uint8_t> getReceived();
  
  bool connected;

private:
  MockNetClient();
  static std::unique_ptr<MockNetClient> instancePtr;

  MessageHandler message_handler;
  std::queue<std::vector<uint8_t> > Q;
  GenericState gstate;
  int32_t localId = -1;
  
  uint32_t netUnitGenId = 0;
};


#endif
