#ifndef _RESOURCES_H_
#define _RESOURCES_H_

#include <iostream>
#include <memory>
#include <map>
#include <fstream>
#include <assert.h>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "json.hpp"
#include "TextureAtlas.h"
#include "Animator.h"
#include "Map.h"
#include "FontConfig.h"
#include "tinyxml2.h"
#include "sharedtypes.h"

// using namespace tinyxml2;

class Resources {
public:
  ~Resources();
  static Resources* get();

  void loadTexture(std::string name);
  sf::Texture* getTexture(std::string name);

  void loadTextureAtlas(std::string name);
  TextureAtlas* getTextureAtlas(std::string name);

  void loadAnimator(std::string name);
  Animator* getAnimator(std::string name);

  void loadMap(std::string name);
  Map* getMap(std::string name);

  void loadFontConfig(std::string name);
  FontConfig* getFontConfig(std::string name);

  void loadShader(std::string name);
  sf::Shader* getShader(std::string name);

  void loadMusic(std::string name);
  sf::Music* getMusic(std::string name);

  void loadSound(std::string name);
  sf::Sound* getSound(std::string name);

private:
  Resources();
  static std::unique_ptr<Resources> instancePtr;

  std::map<std::string, sf::Texture> textures;
  std::map<std::string, std::unique_ptr<TextureAtlas> > texturesAtlas;
  std::map<std::string, std::unique_ptr<Animator> > animators;
  std::map<std::string, std::unique_ptr<Map> > maps;
  std::map<std::string, std::unique_ptr<FontConfig> > fontConfigs;
  std::map<std::string, std::unique_ptr<sf::Shader> > shaders;
  std::map<std::string, std::unique_ptr<sf::Music> > musics;
  std::map<std::string, std::unique_ptr<sf::Sound> > sounds;
  std::map<std::string, std::unique_ptr<sf::SoundBuffer> > soundbuffers;
};

#endif
