#include "Window.h"

Window::Window() {}
Window::~Window() {}

void Window::start(std::string name, int32_t width, int32_t height) {

  LOG("Window start");
  sfRenderWindow = std::make_unique<sf::RenderWindow>(
    sf::VideoMode(width, height), name);

  sfRenderWindow->setVerticalSyncEnabled(true);
//  sfRenderWindow->setFramerateLimit(60);
}

void Window::update() {

  if (sfRenderWindow == nullptr) {
    LOG("ERROR: sf render window not initialized");
    return;
  }

  InputState::get()->updateState(sfRenderWindow.get());

  if (InputState::get()->closed) {
    sfRenderWindow->close();
  }
}


bool Window::isOpen() {

  return sfRenderWindow->isOpen();

}

void Window::setIcon(std::string name) {
  LOG("window set icon");
  if ( ! icon.loadFromFile(name)) {
    LOG("ERROR: icon window not found in " + name);
  }
  sfRenderWindow->
    setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
}

void Window::showMouse(bool v) {
  sfRenderWindow->setMouseCursorVisible(v);
}

float Window::getElapsedTime() {
  float t = clock.getElapsedTime().asSeconds();
  clock.restart();
  return t;
}

void Window::close() {
  sfRenderWindow->close();
}


sf::RenderWindow* Window::getSfRenderWindow() {
  return sfRenderWindow.get();
}




