#include "MessageHandler.h"

MessageHandler::MessageHandler() {}
MessageHandler::~MessageHandler() {}

void MessageHandler::push(char* buffer, size_t bytes_received) {

  uint32_t expected_size = 0;
  for (uint32_t i = 0; i < bytes_received; i++) {

    if (data_expected_size.size() < 4) {
      data_expected_size.push_back(buffer[ i ]);
      continue;
    }

    data.push_back(buffer[ i ]);
  }

  if (data_expected_size.size() < 4) {
    return;
  }

  expected_size =  (data_expected_size[ 0 ] << 24);
  expected_size += (data_expected_size[ 1 ] << 16);
  expected_size += (data_expected_size[ 2 ] << 8);
  expected_size += (data_expected_size[ 3 ] << 0);

  if (data.size() >= expected_size) {

    // save future data
    for (int32_t i = expected_size; i < data.size(); i++) {
      future_data.push_back(data[ i ]);
    }
    data.resize(expected_size);

    q.push(data);
    data_expected_size.clear();
    data.clear();
  }

  if (future_data.size() > 0) {
    std::vector<uint8_t> recursive_fd = future_data;
    future_data.clear();
    push((char*)&recursive_fd[0], recursive_fd.size());
  }
}




