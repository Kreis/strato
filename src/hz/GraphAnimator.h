#ifndef _GRAPH_ANIMATOR_H_
#define _GRAPH_ANIMATOR_H_

#include <iostream>
#include <map>

#include "NodeAnimator.h"

class GraphAnimator {
friend class Animator;
friend class Resources;
public:
  ~GraphAnimator();

  std::vector<NodeAnimator> nodes;

  std::string getUpdatedAnimation(
    const std::map<std::string, std::string>& status);

  int32_t nodeId;

private:
  GraphAnimator();
  std::vector<bool> visited;
};

#endif
