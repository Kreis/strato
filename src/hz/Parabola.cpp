#include "Parabola.h"

Parabola::Parabola() {
  alive = false;
}

void Parabola::init(
  Body* body,
  const vf& time_to_peak,
  const vf& height,
  const vf& current_time,
  const TypeParabola& typeparabola,
  vf limit_time
  ) {
  alive = true;

  this->time_to_peak    = time_to_peak;
  this->height          = height;
  this->current_time    = current_time;
  this->typeparabola    = typeparabola;
  this->limit_time      = limit_time;

  g = (this->height * -2) / (this->time_to_peak * this->time_to_peak);
  v0 = -(g * this->time_to_peak);

  switch (this->typeparabola) {
    case TypeParabola::VERTICAL:
      initial_position = body->getY() - parabola_formula();
      break;
    case TypeParabola::VERTICAL_INVERTED:
      initial_position = body->getY() + parabola_formula();
      break;
    case TypeParabola::HORIZONTAL:
      initial_position = body->getX() - parabola_formula();
      break;
    case TypeParabola::HORIZONTAL_INVERTED:
      initial_position = body->getX() + parabola_formula();
      break;
  }
}

bool Parabola::update(const vf& delta_time, Body* body) {
  if ( ! alive) {
    return false;
  }
  if ( ! update_time(delta_time)) {
    alive = false;
    return false;
  }
  if (is_colliding(body)) {
    alive = false;
    return false;
  }

  static vf parabola_result;
  parabola_result = parabola_formula();

  switch (typeparabola) {
    case TypeParabola::VERTICAL:
      body->moveToY((initial_position + parabola_result).get());
      break;
    case TypeParabola::VERTICAL_INVERTED:
      body->moveToY((initial_position - parabola_result).get());
      break;
    case TypeParabola::HORIZONTAL:
      body->moveToX((initial_position + parabola_result).get());
      break;
    case TypeParabola::HORIZONTAL_INVERTED:
      body->moveToX((initial_position - parabola_result).get());
      break;
  }
  
  return true;
}

// private 
bool Parabola::update_time(const vf& delta_time) {
  if (limit_time != -1 && current_time >= limit_time)
    return false;

  current_time += delta_time;

  if (limit_time != -1 && current_time >= limit_time)
    current_time = limit_time;

  return true;
}

bool Parabola::is_colliding(Body* body) {
  static bool to_down;
  static bool to_up;
  static bool to_left;
  static bool to_right;

  to_down = 
  (typeparabola == TypeParabola::VERTICAL && current_time < time_to_peak) ||
  (typeparabola == TypeParabola::VERTICAL_INVERTED && current_time > time_to_peak);
  if (to_down)
    return body->collidingDown;

  to_up =
  (typeparabola == TypeParabola::VERTICAL_INVERTED && current_time < time_to_peak) || 
  (typeparabola == TypeParabola::VERTICAL && current_time > time_to_peak);
  if (to_up)
    return body->collidingUp;

  to_left =
  (typeparabola == TypeParabola::HORIZONTAL_INVERTED && current_time < time_to_peak) ||
  (typeparabola == TypeParabola::HORIZONTAL && current_time > time_to_peak);
  if (to_left)
    return body->collidingLeft;

  to_right =
  (typeparabola == TypeParabola::HORIZONTAL_INVERTED && current_time < time_to_peak) ||
  (typeparabola == TypeParabola::HORIZONTAL && current_time > time_to_peak);
  if (to_right)
    return body->collidingRight;

  return false;
}

vf Parabola::parabola_formula() {
   return (v0 * current_time) + (g * 0.5) * (current_time * current_time);
}


