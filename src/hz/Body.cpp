#include "Body.h"

Body::Body() {}
Body::~Body() {}

void Body::init(vf x, vf y, vf width, vf height, bool isDynamic) {
  this->x = x;
  this->y = y;
  this->width = width;
  this->height = height;
  alive = true;
  buried = false;
  this->isDynamic = isDynamic;
  slopePriority = false;
  colliding = false;
  enabled = true;
}

vf Body::getX() const {
	return x;
}
vf Body::getY() const {
	return y;
}
vf Body::getWidth() const {
	return width;
}
vf Body::getHeight() const  {
	return height;
}

bool Body::isEnabled() const {
  return enabled;
}

void Body::setPosition(float x, float y) {
  this->x = x;
  this->y = y;
  tryingMove.x = 0.f;
  tryingMove.y = 0.f;

  collidingLeft = false;
  collidingRight = false;
  collidingUp = false;
  collidingDown = false;
}

void Body::setPositionX(float x) {
  this->x = x;
  tryingMove.x = 0.f;
}

void Body::setPositionY(float y) {
  this->y = y;
  tryingMove.y = 0.f;
}

void Body::setType(int32_t v) {
  this->type = v;
}

void Body::setEnabled(bool v) {
  this->enabled = v;
}

void Body::move(vf x, vf y) {
	tryingMove.x += x;
	tryingMove.y += y;
}

void Body::moveTo(vf x, vf y) {
  tryingMove.x = x - this->x;
  tryingMove.y = y - this->y;
}

void Body::moveToX(vf x) {
  tryingMove.x = x - this->x;
}

void Body::moveToY(vf y) {
  tryingMove.y = y - this->y;
}

bool Body::isColliding() const {
  return colliding;
}

void Body::off() {
	alive = false;
}
