#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <memory>

#include "sharedtypes.h"

class Camera {
public:
  ~Camera();

  static Camera* get();

  Rect<float> bounds;

  // x = left right, y = top bottom
  Vec<int32_t> banners;

private:
  Camera();
  static std::unique_ptr<Camera> instancePtr;
};

#endif
