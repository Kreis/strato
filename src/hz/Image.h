#ifndef _IMAGE_H_
#define _IMAGE_H_

#include "Resources.h"
#include "sharedtypes.h"
#include "Sprite.h"

// Image class encapsulates sprite with extra information
// loading texture in Resources by string
// taking Texture Rect from Texture Atlas

class Image {
public:
  Image();
  ~Image();

  // for texture Atlas
  void init(
    std::string textureAtlas,
    std::string texturePiece,
    Rect<int32_t> bounds);    
  // for direct texture
  void init(
    std::string texturePath,
    Rect<int32_t> bounds);
  // for non texture
  void init(
    std::string noTextureName,
    Rect<int32_t> color,
    Rect<int32_t> bounds);

  Sprite* getSprite();

private:
  bool initialized;
  Sprite sprite;
};

#endif
