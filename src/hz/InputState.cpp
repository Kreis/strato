#include "InputState.h"

InputState* InputState::instancePtr = nullptr;
InputState::InputState() {
	std::fill(
		this->bufferKeys.begin(),
		this->bufferKeys.end(),
		0
	);
	std::fill(
		this->bufferMouseButtons.begin(),
		this->bufferMouseButtons.end(),
		0
	);
	this->closed = false;
	this->resized = false;
	this->textEntered = '\0';
  this->currentScreenSize = { 0, 0};
}
InputState::~InputState() {
	if (this->instancePtr != nullptr) {
		this->instancePtr = nullptr;
	}
}

void InputState::updateState(sf::RenderWindow* sfRenderWindow) {

	// update status typed to pressed
	for (uint8_t i = 0; i < sf::Keyboard::KeyCount; i++) {
		this->bufferKeys[ i ] |=
			(this->bufferKeys[ i ] & 1) << 1;
	}
	for (uint8_t i = 0; i < sf::Mouse::ButtonCount; i++) {
		this->bufferMouseButtons[ i ] |= 
			(this->bufferMouseButtons[ i ] & 1) << 1;
	}

	this->vecKeyReleased.clear();
	this->vecMouseReleased.clear();
	this->resized = false;
	this->textEntered = 0;
  if (this->currentScreenSize.x == 0) {
    this->currentScreenSize.x = sfRenderWindow->getSize().x;
    this->currentScreenSize.y = sfRenderWindow->getSize().y;
  }

	while (sfRenderWindow->pollEvent(this->ev)) switch (this->ev.type) {
	case sf::Event::Closed:
		this->closed = true;
		break;

	case sf::Event::KeyPressed:
    if (this->ev.key.code >= this->bufferKeys.size())
      break;
		this->bufferKeys[ this->ev.key.code ] |= 1;
		break;

	case sf::Event::KeyReleased:
    if (this->ev.key.code >= this->bufferKeys.size())
      break;
		this->bufferKeys[ this->ev.key.code ] = 0;
		this->vecKeyReleased.push_back(this->ev.key.code);
		break;
	
	case sf::Event::MouseButtonPressed:
    if (this->ev.mouseButton.button >= this->bufferMouseButtons.size())
      break;
		this->bufferMouseButtons[ this->ev.mouseButton.button ] |= 1;
		break;
	
	case sf::Event::MouseButtonReleased:
    if (this->ev.mouseButton.button >= this->bufferMouseButtons.size())
      break;
		this->bufferMouseButtons[ this->ev.mouseButton.button ] = 0;
		this->vecMouseReleased.push_back(this->ev.mouseButton.button);
		break;
	
	case sf::Event::TextEntered:
		this->textEntered = this->ev.text.unicode;
		break;

  	case sf::Event::LostFocus:
		std::fill(
			this->bufferKeys.begin(),
			this->bufferKeys.end(),
			0
		);
		std::fill(
			this->bufferMouseButtons.begin(),
			this->bufferMouseButtons.end(),
			0
		);
		break;
	
	case sf::Event::Resized:
		this->resized = true;
    this->currentScreenSize.x = sfRenderWindow->getSize().x;
    this->currentScreenSize.y = sfRenderWindow->getSize().y;
	}

	sf::Vector2i sfmlMousePosition =
    sf::Mouse::getPosition(*sfRenderWindow);
	this->mousePosition.x = sfmlMousePosition.x;
	this->mousePosition.y = sfmlMousePosition.y;
}

bool InputState::isPressed(KeyButton key) const {
  sf::Keyboard::Key sfKey =
    InputDataType::getSfmlKey(key);
	return this->bufferKeys[ sfKey ] > 0;
}

bool InputState::isTyped(KeyButton key) const {
  sf::Keyboard::Key sfKey =
    InputDataType::getSfmlKey(key);
	return this->bufferKeys[ sfKey ] == 1;
}

bool InputState::isReleased(KeyButton key) const {
  sf::Keyboard::Key sfKey =
    InputDataType::getSfmlKey(key);
	for (const uint8_t& i : this->vecKeyReleased) {
		if (i == sfKey)
			return true;
	}

	return false;
}

bool InputState::isPressed(MouseButton mouseButton) const {
  sf::Mouse::Button button = InputDataType::getSfmlMouseButton(mouseButton);
	return this->bufferMouseButtons[ button ] > 0;
}

bool InputState::isTyped(MouseButton mouseButton) const {
  sf::Mouse::Button button = InputDataType::getSfmlMouseButton(mouseButton);
	return this->bufferMouseButtons[ button ] == 1;
}

bool InputState::isReleased(MouseButton mouseButton) const {
  sf::Mouse::Button button = InputDataType::getSfmlMouseButton(mouseButton);
	for (const uint8_t& i : this->vecMouseReleased) {
		if (i == button)
			return true;
	}

	return false;
}

uint32_t InputState::getTextEntered() const {
	return this->textEntered;
}

Vec<int32_t> InputState::getCurrentScreenSize() const {
  return this->currentScreenSize;
}

bool InputState::isResized() const {
	return this->resized;
}

Vec<int32_t> InputState::getMousePosition() {
	return this->mousePosition;
}

InputState* InputState::get() {
	if (instancePtr == nullptr) {
		instancePtr = new InputState();
	}

	return instancePtr;
}

