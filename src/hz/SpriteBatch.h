#ifndef _SPRITE_BATCH_H_
#define _SPRITE_BATCH_H_

#include <iostream>
#include <vector>

#include <SFML/Graphics.hpp>

#include "Sprite.h"
#include "Window.h"
#include "Camera.h"
#include "sharedtypes.h"

class SpriteBatch {
public:
  ~SpriteBatch();
  static SpriteBatch* get();

  void init(std::vector<std::string> _texturePriorities);

  void draw(Sprite* sprite);
  void fixVecVertex(std::string _textureName,
    std::vector<sf::Vertex>* _vecVertex);

  void setEnableVecVertex(std::string _textureName, bool enable);
  void clearVecVertex(std::string _textureName);
  void setShaderToVecVertex(std::string _textureName, std::string _shaderName);
  void setNullTextureVecVertex(std::string _textureName);
  void setCameraFreeVecVertex(std::string _textureName, bool enable);

  void flush(Window* window);

private:
  SpriteBatch();

  int32_t getTexturePriorityId(std::string textureName);
  static std::unique_ptr<SpriteBatch> instancePtr;

  std::vector<std::string> texturePriorities; 

  std::vector<std::vector<sf::Vertex> > vecVertex;
  std::vector<bool> isFixVecVertex;
  std::vector<bool> isEnableVecVertex;
  std::vector<Vec<float> > cameraVecVertex;
  std::vector<std::string> shaderInVecVertex;
  std::vector<bool> isNullTextureVecVertex;
  std::vector<bool> isCameraFreeVecVertex;
  sf::RenderStates rs;
};

#endif
