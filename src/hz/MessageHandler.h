#ifndef _MESSAGEHANDLER_H_
#define _MESSAGEHANDLER_H_

#include <iostream>
#include <queue>
#include <vector>

class MessageHandler {
public:
  MessageHandler();
  ~MessageHandler();

  std::queue<std::vector<uint8_t> > q;

  void push(char* buffer, size_t bytes_received);

private:
  std::vector<uint8_t> data_expected_size;
  std::vector<uint8_t> data;
  std::vector<uint8_t> future_data;
};


#endif
