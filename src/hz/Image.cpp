#include "Image.h"

Image::Image() {}
Image::~Image() {}

// for texture Atlas
void Image::init(
  std::string textureAtlas,
  std::string texturePiece,
  Rect<int32_t> bounds) {

  TextureAtlas* ta = Resources::get()->getTextureAtlas(textureAtlas);
  sprite.textureName = ta->textureName;
  sprite.bounds = bounds;
  sprite.textureRect = ta->getPieceRect(texturePiece);

  initialized = true;
}

// for direct texture
void Image::init(
  std::string texturePath,
  Rect<int32_t> bounds) {

  Resources::get()->loadTexture(texturePath);

  sprite.textureName = texturePath;
  sprite.bounds = bounds;
  sprite.textureRect = { 0, 0, bounds.w, bounds.h };

  initialized = true;
}

// for non texture
void Image::init(
  std::string noTextureName,
  Rect<int32_t> color,
  Rect<int32_t> bounds) {

  sprite.textureName = noTextureName;
  sprite.bounds = bounds;
  sprite.color = color;

  initialized = true;
}

Sprite* Image::getSprite() {
  if ( ! initialized) {
    LOG("ERROR: sprite in Image not initialized");
  }

  return &sprite;
}





