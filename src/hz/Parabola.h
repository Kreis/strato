#ifndef _PARABOLA_H_
#define _PARABOLA_H_

#include <memory>
#include <iostream>

#include "Body.h"
#include "sharedtypes.h"

enum class TypeParabola {
  VERTICAL,
  VERTICAL_INVERTED,
  HORIZONTAL,
  HORIZONTAL_INVERTED
};

class Parabola {
public:
  Parabola();

  void init(
    Body* body,
    const vf& time_to_peak,
    const vf& height,
    const vf& current_time,
    const TypeParabola& typeparabola,
    vf limit_time = -1
    );

  bool update(const vf& delta_time, Body* body);

private:
  vf initial_position;
  vf time_to_peak;
  vf height;
  vf current_time;
  TypeParabola typeparabola;
  vf limit_time;

  vf v0;
  vf g;

  bool update_time(const vf& delta_time);
  bool is_colliding(Body* body);

  vf parabola_formula();

  bool alive;
};


#endif
