#ifndef _FACTORY_NET_H_
#define _FACTORY_NET_H_

#include "MockNetClient.h"
#include "hz/Hz.h"

#define MOCKED

class FactoryNet {
public:
  ~FactoryNet();
  static std::unique_ptr<FactoryNet> instancePtr;

  static FactoryNet* get();

  uint8_t getB01(int32_t n);
  uint8_t getB02(int32_t n);
  uint8_t getB03(int32_t n);
  uint8_t getB04(int32_t n);

#ifdef MOCKED
  MockNetClient* net();
#else
  NetClient* net();
#endif

private:
  FactoryNet();
};

#endif
