#include "Aplayer.h"

Aplayer::Aplayer() {}
Aplayer::~Aplayer() {}

void Aplayer::init(Rect<int32_t> color, Rect<int32_t> rect) {
  Resources::get()->loadAnimator("data/animators/aplayer.json");

  Animator* animator =
    Resources::get()->getAnimator("data/animators/aplayer.json");
  animatorSession = animator->getNewSession();

  animator->runAnimation("jumping_unassigned", animatorSession);

  this->color = color;
  this->bounds = rect;
  movingTo.x = rect.x;
  movingTo.y = rect.y;

  defaultPosition = rect.xy;

  speed = 128.0f;

  // true if mouse click aplayer at least one time
  clicked = false;

  // true when Lobby assign this aplayer to a controller
  assigned = false;

  Mouse::get()->addObserver(weak_from_this());
}

void Aplayer::leftClick(const Vec<int32_t>& p) {
  if (bounds.contains(p)) {
    clicked = true;
  }
}

void Aplayer::rightClick(const Vec<int32_t>& p) {

}

void Aplayer::movingOver(const Vec<int32_t>& p) {

}

void Aplayer::moveTo(Vec<int32_t> p) {
  movingTo = p;
}

void Aplayer::setPosition(Vec<int32_t> p) {
  bounds.x = p.x;
  bounds.y = p.y;
  movingTo.x = p.x;
  movingTo.y = p.y;
}

void Aplayer::update(float deltatime) {
  Animator* animator =
    Resources::get()->getAnimator("data/animators/aplayer.json");

  if (assigned) {
    animator->runAnimation("jumping", animatorSession);
  } else {
    animator->runAnimation("jumping_unassigned", animatorSession);
    moveTo(defaultPosition);
  }

  animator->update(deltatime, animatorSession);

  Vec<float> movingToF = movingTo;

  Vec<float> direction = movingToF - bounds.xy;
  if (direction.x == 0 && direction.y == 0) {
    return;
  }
  direction = direction.normalized();

  float step = speed * deltatime;
  if (bounds.xy.distance(movingToF) < step) {
    bounds.xy = movingTo;
  } else {
    bounds.xy = bounds.xy + direction * step;
  }
}

void Aplayer::draw() {
  Animator* animator =
    Resources::get()->getAnimator("data/animators/aplayer.json");

  animator->bounds.x = bounds.x;
  animator->bounds.y = bounds.y - 32;
  animator->setColor(color);

  animator->draw(animatorSession);
}



