#ifndef _FIELD_H_
#define _FIELD_H_

#include <iostream>
#include <vector>

#include "hz/Hz.h"

#include "GlobalConfig.h"
#include "Unit.h"
#include "FactoryNet.h"
#include "Mouse.h"

enum class ResType {
  RED, BLUE, GREEN, BLACK, WHITE, NOT_RES
};

struct Res {
  Vec<int32_t> position;
  uint32_t amount;
  ResType type; 
};

class Field : public MouseObserver {
public:
  Field();
  ~Field();

  void init();
  void update(float deltatime);
  void draw();

  void leftClick(const Vec<int32_t>& p) override;
  void rightClick(const Vec<int32_t>& p) override;
  void movingOver(const Vec<int32_t>& p) override;

  std::vector<Res> res;
  Image imageRes;

private:
  void updateRemote();
  int32_t getNewUnit();
  void activeUnit(int32_t i, uint8_t x, uint8_t y);
  std::map<uint32_t, int32_t> unitsMap;
  std::vector<Unit> units;

  void updateUnits();

  int32_t movingHor = 0;
  int32_t movingVer = 0;

  bool selectedMenu;
  bool selectedUnit;

  Image lightTile;
  Image imageDuplicate;

  float cameraSpeed;
};


#endif
