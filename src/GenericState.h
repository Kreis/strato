#ifndef _GENERICSTATE_H_
#define _GENERICSTATE_H_

#include <vector>
#include <iostream>

#define M_ASSIGN 'A'
#define M_MOVE_TO 'B'
#define M_READY 'C'
#define M_ACTIVE_UNIT 'D'

struct NetUnit {
  uint32_t id;
  bool active;
  uint8_t x;
  uint8_t y;
};

struct NetUser {
  bool confirmed = false;
  bool assigned = false;
  bool moved = false;
  uint8_t colortype = 0;
  std::string username = "blank";
  uint8_t lobbyX = 0; // tile
  uint8_t lobbyY = 0; // tile
  uint32_t redResources = 0;
  uint32_t blueResources = 0;
  uint32_t greenResources = 0;

  std::vector<NetUnit> units;
};

struct GenericState {
  uint32_t lastSerializeId = 0; // NOT serialize this value
  
  int8_t state = 1;
  std::vector<NetUser> users = std::vector<NetUser>(4);

  int8_t last = 64;

  void serialize(std::vector<uint8_t>& data) {

    for (int32_t i = 0; i < 4; i++) {
      NetUser& u = users[ i ];
      data.push_back(u.confirmed);
      data.push_back(u.assigned);
      data.push_back(u.moved);
      data.push_back(u.colortype);
  
      data.push_back(u.username.length());
      for (char c : u.username) {
        data.push_back(c);
      }
  
      data.push_back(u.lobbyX);
      data.push_back(u.lobbyY);
      data.push_back(u.redResources);
      data.push_back(u.blueResources);
      data.push_back(u.greenResources);

      data.push_back(u.units.size());
      for (uint32_t i = 0; i < u.units.size(); i++) {
        data.push_back(u.units[ i ].id);
        data.push_back(u.units[ i ].active);
        data.push_back(u.units[ i ].x);
        data.push_back(u.units[ i ].y);
      }
    }

    data.push_back(state);
    data.push_back(last);
  }

  void deserialize(std::vector<uint8_t>& data) {

    uint32_t id = 0;
    for (int32_t i = 0; i < 4; i++) {
      NetUser& u = users[ i ];
      u.confirmed = data[ id++ ];
      u.assigned = data[ id++ ];
      u.moved = data[ id++ ];
      u.colortype = data[ id++ ];

      int32_t username_len = data[ id++ ];
      u.username = "";
      for (int32_t j = 0; j < username_len; j++) {
        u.username += (char)data[ id++ ];
      }

      u.lobbyX = data[ id++ ];
      u.lobbyY = data[ id++ ];
      u.redResources = data[ id++ ];
      u.blueResources = data[ id++ ];
      u.greenResources = data[ id++ ];

      uint32_t users_size = data[ id++ ];
      for (uint32_t i = 0; i < users_size; i++) {
        NetUnit netUnit;
        netUnit.id = data[ id++ ];
        netUnit.active = data[ id++ ];
        netUnit.x = data[ id++ ];
        netUnit.y = data[ id++ ];
        u.units.push_back(netUnit);
      }
    }

    state = data[ id++ ];
    // ignoring data[ id ] = last

    lastSerializeId++;
  }

};


#endif
