#ifndef _LOCAL_CONTROLLER_H_
#define _LOCAL_CONTROLLER_H_

#include <memory>

#include "hz/Hz.h"

#include "Aplayer.h"
#include "Mouse.h"
#include "GenericState.h"
#include "FactoryNet.h"

class LocalController : public MouseObserver {
public:
  LocalController();
  ~LocalController();

  void init(std::shared_ptr<Aplayer> aplayer);

  void leftClick(const Vec<int32_t>& p) override;
  void rightClick(const Vec<int32_t>& p) override;
  void movingOver(const Vec<int32_t>& p) override;

  void update(float deltatime);

  Vec<int32_t> getMapTile(const Vec<int32_t>& p);

  bool isInitialized();
private:
  std::shared_ptr<Aplayer> aplayer;
  bool initialized;

  Image lightTile;
};

#endif
