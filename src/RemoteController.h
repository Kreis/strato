#ifndef _REMOTE_CONTROLLER_H_
#define _REMOTE_CONTROLLER_H_

#include <vector>

#include "hz/Hz.h"

#include "Aplayer.h"

#include "GlobalConfig.h"
#include "GenericState.h"
#include "FactoryNet.h"

class RemoteController {
public:
  RemoteController();
  ~RemoteController();

  void update(float deltatime,
    std::vector<std::shared_ptr<Aplayer> >& aplayers);

  GenericState gstate;

  uint32_t lastSID = 0;
};

#endif
