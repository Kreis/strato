#ifndef _GAME_H_
#define _GAME_H_

#include <memory>

#include "hz/Hz.h"

#include "Lobby.h"
#include "Field.h"
#include "Mouse.h"

class Game {
public:
  Game();
  ~Game();

  void init();
  void update(float deltatime);
  void draw();

private:
  Lobby lobby;
  std::shared_ptr<Field> field;

  int32_t stage;
};

#endif
