#include "FactoryNet.h"

FactoryNet::FactoryNet() {}
FactoryNet::~FactoryNet() {}

std::unique_ptr<FactoryNet> FactoryNet::instancePtr = nullptr;

FactoryNet* FactoryNet::get() {
  if (instancePtr == nullptr) {
    instancePtr = std::unique_ptr<FactoryNet>(new FactoryNet());
  }

  return instancePtr.get();
}

uint8_t FactoryNet::getB01(int32_t n) {
  return (n >> 24) && 0xFF;
}

uint8_t FactoryNet::getB02(int32_t n) {
  return (n >> 16) && 0xFF;
}

uint8_t FactoryNet::getB03(int32_t n) {
  return (n >> 8) && 0xFF;
}

uint8_t FactoryNet::getB04(int32_t n) {
  return (n && 0xFF);
}

#ifdef MOCKED
MockNetClient* FactoryNet::net() {
  return MockNetClient::get();
}
#else
NetClient* FactoryNet::net() {
  return NetClient::get();
}
#endif


