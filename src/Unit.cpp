#include "Unit.h"

Unit::Unit() {}
Unit::~Unit() {}

void Unit::init(uint32_t id, Vec<int32_t> pos, int32_t unitType) {
  this->unitType = unitType;
  this->id = id;
  
  Resources::get()->loadAnimator("data/animators/aplayer.json"); 
  Animator* animator =
    Resources::get()->getAnimator("data/animators/aplayer.json");
  animatorSession = animator->getNewSession();  
  animator->runAnimation("jumping", animatorSession);
  if (unitType == 1) {
    this->color = GlobalConfig::get()->colorGreen;
  } else if (unitType == 2) {
    this->color = GlobalConfig::get()->colorRed;
  } else if (unitType == 3) {
    this->color = GlobalConfig::get()->colorBlue;
  } else if (unitType == 4) {
    this->color = GlobalConfig::get()->colorPink;
  }

  speed = 128.0f;
  selected = false;

  bounds.xy = pos;
  bounds.wh = 32;
  movingTo = pos;
  active = false;
}

void Unit::update(float deltatime) {
  Animator* animator =
    Resources::get()->getAnimator("data/animators/aplayer.json");

  animator->update(deltatime, animatorSession);

  Vec<float> direction = movingTo - bounds.xy;
  if (direction.x == 0 && direction.y == 0) {
    return;
  }
  direction = direction.normalized();
  float step = speed * deltatime;
  if (bounds.xy.distance(movingTo) < step) {
    bounds.xy = movingTo;
  } else {
    bounds.xy = bounds.xy + direction * step;
  }
}

void Unit::draw() {
  Animator* animator =
    Resources::get()->getAnimator("data/animators/aplayer.json");

  animator->setColor(color);
  animator->bounds.xy = bounds.xy;
  animator->bounds.y -= 32;
  animator->draw(animatorSession);
}

void Unit::moveTo(Vec<int32_t> p) {
  movingTo = p;
}


